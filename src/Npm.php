<?php

namespace NpmPhp;

/**
 * Class Npm
 * @package NpmPhp
 */
class Npm
{
    /**
     * @var null|string
     */
    private $webRoot = null;

    /**
     * @var null|string
     */
    private $mapConfigPath = null;

    /**
     * @var null | \NpmPhp\NpmMap
     */
    private $NpmMap = null;

    /**
     * Npm constructor.
     *
     * @param string      $webRoot
     * @param string|null $mapConfigPath
     */
    public function __construct(string $webRoot, string $mapConfigPath = null)
    {
        $this->webRoot       = $webRoot;
        $this->mapConfigPath = $mapConfigPath;
    }

    /**
     * @return \NpmPhp\NpmMap|null
     * @throws \Exception
     */
    public function map()
    {
        if (empty($this->NpmMap) && ! empty($this->mapConfigPath)) {
            $this->NpmMap = new NpmMap($this->webRoot, $this->mapConfigPath);
        }

        if (empty($this->NpmMap)) {
            throw new \Exception('NPM map is not specified');
        }

        return $this->NpmMap;
    }

    /**
     * @param string $fullPath
     *
     * @return string
     */
    public function script(string $fullPath)
    {
        return sprintf(
            '<script type="text/javascript" src="%snode_modules/%s.js"></script>',
            $this->webRoot,
            $fullPath
        );
    }

    /**
     * @param string $fullPath
     *
     * @return string
     */
    public function stylesheet(string $fullPath)
    {
        return sprintf(
            '<link rel="stylesheet" type="text/css" href="%snode_modules/%s.css"/>',
            $this->webRoot,
            $fullPath
        );
    }
}
