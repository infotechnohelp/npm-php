<?php

namespace NpmPhp;

/**
 * Class NpmMap
 * @package NpmPhp
 */
class NpmMap extends Npm
{
    /**
     * @var mixed|null
     */
    public $mapConfig = null;

    /**
     * NpmMap constructor.
     *
     * @param string $webRoot
     * @param string $mapConfigPath
     */
    public function __construct(string $webRoot, string $mapConfigPath)
    {
        $this->mapConfig = json_decode(file_get_contents($mapConfigPath . '.json'), true);

        parent::__construct($webRoot);
    }

    /**
     * @param string $packagePath
     * @param string $type
     *
     * @return null|string
     */
    private function selectActionByType(string $packagePath, string $type)
    {
        switch ($type) {
            case 'js':
                return parent::script($packagePath);
                break;
            case 'css':
                return parent::stylesheet($packagePath);
                break;
            default:
                return null;
        }
    }

    /**
     * @param string $packageTitle
     * @param string $type
     *
     * @return array|null
     */
    private function getPackageSet(string $packageTitle, string $type)
    {
        $result = [];
        $except = false;

        if (strpos($packageTitle, ':') === false && ! is_array($this->mapConfig[$type][$packageTitle])) {
            return null;
        }

        if (strpos($packageTitle, ':') === false) {
            foreach ($this->mapConfig[$type][$packageTitle] as $packageKey => $packagePath) {
                $result[] = $packageKey;
            }

            return $result;
        }

        $packageTitleArray = explode(':', $packageTitle);
        $packageTitle      = $packageTitleArray[0];
        $subPackages       = explode(',', $packageTitleArray[1]);


        foreach ($subPackages as $index => $subPackage) {
            $subPackages[$index] = trim($subPackage);
        }

        if (substr($subPackages[0], 0, 1) === '-') {
            $except = true;

            $subPackages[0] = substr($subPackages[0], 1);
        }

        foreach ($this->mapConfig[$type][$packageTitle] as $packageKey => $packagePath) {
            if ($except) {
                if (! in_array($packageKey, $subPackages, true)) {
                    $result[] = $packageKey;
                }
            } else {
                if (in_array($packageKey, $subPackages, true)) {
                    $result[] = $packageKey;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $packageTitle
     * @param string $type
     *
     * @return null|string
     */
    private function returnPackagePaths(string $packageTitle, string $type)
    {
        $result = [];

        $packageSet = $this->getPackageSet($packageTitle, $type);

        if ($packageSet === null) {
            return $this->selectActionByType($this->mapConfig[$type][$packageTitle], $type);
        } else {
            foreach ($packageSet as $subPackageTitle) {
                $cleanPackageTitle = trim(explode(':', $packageTitle)[0]);
                $result[]          = $this->selectActionByType(
                    $this->mapConfig[$type][$cleanPackageTitle][$subPackageTitle],
                    $type
                );
            }
        }

        return implode("\n", $result);
    }

    /**
     * @param string $packageTitle
     *
     * @return null|string
     */
    public function script(string $packageTitle)
    {
        return $this->returnPackagePaths($packageTitle, 'js');
    }

    /**
     * @param string $packageTitle
     *
     * @return string
     */
    public function stylesheet(string $packageTitle)
    {
        return $this->returnPackagePaths($packageTitle, 'css');
    }

    /**
     * @param string     $type
     * @param array|null $packageTitles
     * @param bool       $expect
     *
     * @return string
     */
    private function returnMapPaths(string $type, array $packageTitles = null, bool $expect = false)
    {
        $result = [];

        if (empty($packageTitles)) {
            foreach ($this->mapConfig[$type] as $packageTitle => $value) {
                $result[] = $this->returnPackagePaths($packageTitle, $type);
            }
        } else {
            if ($expect) {
                $clearPackageTitles = [];

                foreach ($packageTitles as $index => $fullPackageTitle) {
                    $clearPackageTitles[] = trim(explode(':', $fullPackageTitle)[0]);
                }

                foreach ($this->mapConfig[$type] as $packageTitle => $value) {
                    if (in_array($packageTitle, $clearPackageTitles, true)) {
                        $index = array_search($packageTitle, $clearPackageTitles);

                        $packageSet = $this->getPackageSet($packageTitles[$index], $type);

                        foreach ($this->mapConfig[$type][$packageTitle] as $packageKey => $packageValue) {
                            if (! in_array($packageKey, $packageSet, true)) {
                                $result[] = $this->selectActionByType($packageValue, $type);
                            }
                        }
                    } else {
                        $result[] = $this->returnPackagePaths($packageTitle, $type);
                    }
                }
            } else {
                foreach ($packageTitles as $packageTitle) {
                    $result[] = $this->returnPackagePaths($packageTitle, $type);
                }
            }
        }

        return implode("\n", $result);
    }

    /**
     * @param array|null $packageTitles
     * @param bool       $expect
     *
     * @return string
     */
    public function scripts(array $packageTitles = null, bool $expect = false)
    {
        return $this->returnMapPaths('js', $packageTitles, $expect);
    }

    /**
     * @param array|null $packageTitles
     * @param bool       $expect
     *
     * @return string
     */
    public function stylesheets(array $packageTitles = null, bool $expect = false)
    {
        return $this->returnMapPaths('css', $packageTitles, $expect);
    }
}
