## Change package's title
Change "name" value in `PACKAGE/composer.json`

Replace All in Path `PACKAGE`

PackageSkeleton → AnyPackageName

`composer dump-autoload`

### Tests
Run tests (Package root)

`vendor/bin/phpunit`

### Sniff
Fix (Package root)

`./sniff -f` 

Check for conflicts (Package root)

`./sniff`